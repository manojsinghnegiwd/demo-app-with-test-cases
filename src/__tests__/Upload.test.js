import React from 'react';
import { render } from '@testing-library/react';
import Upload from '../components/Audio/Upload';

test('Upload button renders', () => {
  const { getByText } = render(
    <Upload onUpload={() => {}} onUploadStart={() => {}} />
  );
  const buttonText = getByText(/upload/i);
  expect(buttonText).toBeInTheDocument();
});
