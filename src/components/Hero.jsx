import React from 'react' 

const Hero = () => {
  return (
    <div className="titleContainer">
      <h1 className="titleHeading">
        Sound Tracks
      </h1>
      <h1 className="titleSpan">
        Instantly
      </h1>
      <p className="titleContent">
        Sound Tracks
      </p>
      <p className="titleContent"> 
        <b>Sound Tracks</b>
      </p>
      <p className="titleContentLast"> 
        <span>
          Upload your track and hear it now for free
        </span> 
      </p>
    </div>
  )
}

export default Hero