import React, { useState } from "react";
import { useCallback } from "react";
import { useRef } from "react";
import {LoadingOutlined} from "@ant-design/icons";

import "./Upload.css"

const Upload = ({
    onUpload,
    onUploadStart
}) => {
    const uploadInput = useRef()
    const [ loading, setLoading ] = useState(false);
    const [ error, setError ] = useState(null);

    const handleUpload = useCallback((e) => {
        // if no files don't do anything
        if (!e.target.files.length) {
            return
        }

        if (e.target.files[0].size > 4194304) {
            setError("Please Upload File of Size less than 4MB"); 
            return
        }

        setLoading(true);
        onUploadStart()
        
        //if file found read it as data url
        const file = e.target.files[0]
        const reader = new FileReader()
        reader.readAsDataURL(file)

        reader.onload = () => {
            const { name, type } = file
            const stringifiedTrack = JSON.stringify({
                base64Data: reader.result,
                name,
                type
            }) 
            setTimeout(() => {
                onUpload(stringifiedTrack)
                setLoading(false)
            },2000) 
        }
       
    }, [onUpload, onUploadStart])

    return (
        <>
            <button className="actionButton" onClick={() => uploadInput.current.click()}> 
                { loading ? <LoadingOutlined /> : "UPLOAD" }
            </button>
            <input 
                type="file"
                accept="audio/*" 
                className="uploadInput"
                ref={uploadInput} 
                onChange={handleUpload}
            />
            { 
                error &&
                    <p className="error">
                        <b> {error} </b> 
                    </p>
            }
        </>
    )
}

export default Upload
