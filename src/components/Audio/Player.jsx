import React, { useEffect } from "react"
import { useRef, useState } from "react" 
import { PlayCircleFilled, PauseCircleFilled } from "@ant-design/icons";

import "./Player.css"

const Player = ({
    audioTrack 
}) => {
    const audioRef = useRef()
    const [ playing, setPlaying ] = useState(false)

    useEffect(() => {
        if (!audioTrack) {
            setPlaying(false)
        }
    }, [audioTrack])

    if (!audioTrack) {
        return null
    }
   
    const parsedAudioObj = JSON.parse(audioTrack)
    const { base64Data } = parsedAudioObj

    
    return (
        <>  
            <audio src={base64Data} loop ref={audioRef} className="audioPlayer" />
            <button 
                id="playButton"
                className="playButton actionButton" 
                onClick={() => setPlaying(!playing)} 
            >
                {
                    !playing ? 
                        <PlayCircleFilled 
                            className="playerIcon"
                            id="play"
                            onClick={() => audioRef.current.play()} 
                        /> : 
                        <PauseCircleFilled 
                            className="playerIcon"
                            id="pause"
                            onClick={() => audioRef.current.pause()}
                        />
                }
            </button>
        </>
    )
}

export default Player
