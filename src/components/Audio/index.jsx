import React, { useState } from "react";
import { useCallback } from "react";
import Download from "./Download";
import Upload from "./Upload";
import Player from "./Player";

const Audio = () => {
    const audio = localStorage.getItem("audio")
    const [audioTrack, setAudioTrack] = useState(audio)

    const onUpload = useCallback(uploadedTrack => {
        localStorage.setItem("audio", uploadedTrack)
        setAudioTrack(uploadedTrack)
    }, [setAudioTrack])

    const onDownload =  useCallback(() => {
        setAudioTrack(null)
        localStorage.clear()
    }, [setAudioTrack])

    const onUploadStart = useCallback(() => {
        localStorage.clear()
        setAudioTrack(null)
    }, [setAudioTrack])
    return (
        <>
            <Upload
                onUpload={onUpload}
                onUploadStart={onUploadStart}
            />
            <div className="container"> 
                <Download
                    audioTrack={audioTrack}
                    onDownload={onDownload}
                />
                <Player
                    audioTrack={audioTrack}
                /> 
            </div>
        </>
    )
}

export default Audio
