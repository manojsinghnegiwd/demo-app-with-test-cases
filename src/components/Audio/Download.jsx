import React from "react"
import { useCallback } from "react"
import { convertURIToBinary, downloadFile } from "../../utils/audio"

import "./Download.css"

const Download = ({
    audioTrack,
    onDownload
}) => {
    const handleDownload = useCallback(() => {
        const parsedAudioObj = JSON.parse(audioTrack)
        const { base64Data, name, type } = parsedAudioObj

        let binary = convertURIToBinary(base64Data)
        let blob = new Blob([binary], { type })
        downloadFile(name, blob)
        onDownload()
    }, [audioTrack, onDownload])

    if (!audioTrack) {
        return null
    }

    const parsedAudioObj = JSON.parse(audioTrack)

    return (
        <button className="actionButton mobile-action-button" onClick={handleDownload}> 
            <p className="downloadButtonText">DOWNLOAD</p>
            <span className="downloadFileName">{parsedAudioObj.name}</span>
        </button>
    )
}

export default Download
