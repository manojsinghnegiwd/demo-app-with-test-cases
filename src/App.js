import React from 'react';
import './App.css';
import Audio from './components/Audio';
import Hero from './components/Hero';

function App() {
  return (
    <div className="App">
      <Hero />
      <Audio />
    </div>
  );
}

export default App;
